## INTRODUCTION

The BEF Date filters provides new Better exposed filters plugins to handle dates.

The primary use case for this module is:

- Transform a date range filter (content is between two dates) into a year and month select filters.

## REQUIREMENTS

[Better exposed filters](https://www.drupal.org/project/better_exposed_filters)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Add a view with a date exposed filter that has the operator "Is between"
- Set the view to use better exposed filters and configure the "Year and month for date range" for that filter.

## MAINTAINERS

Current maintainers for Drupal 10:

- Rodrigo Aguilera (rodrigoaguilera - https://www.drupal.org/u/rodrigoaguilera
