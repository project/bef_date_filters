<?php

namespace Drupal\bef_date_filters\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\FilterWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Year and month widget implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "bef_year_month_between",
 *   label = @Translation("Year and month for date range"),
 * )
 */
class YearMonthBetween extends FilterWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable($filter = NULL, array $filter_options = []) : bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $is_applicable = FALSE;

    if ((is_a($filter, 'Drupal\views\Plugin\views\filter\Date') || !empty($filter->date_handler)) && !$filter->isAGroup()) {
      $is_applicable = TRUE;
    }

    return $is_applicable;
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state) : void {
    if (!$form['#id']) {
      return;
    }
    $field_id = $this->getExposedFilterFieldId();

    $element = [];

    // Handle wrapper element added to exposed filters
    // in https://www.drupal.org/project/drupal/issues/2625136.
    $wrapper_id = $field_id . '_wrapper';
    if (!isset($form[$field_id]) && isset($form[$wrapper_id])) {
      $element = &$form[$wrapper_id][$field_id];
    }
    else {
      $element = &$form[$field_id];
    }

    parent::exposedFormAlter($form, $form_state);

    $form['#attached']['library'][] = 'bef_date_filters/year-month-between';
    $form['#attributes']['class'][] = 'bef-date-filter';

    $yearOptions = range(date('Y'), date('Y') - 10);

    $monthOptions = [
      '01' => $this->t('January', [], ['context' => 'Long month name']),
      '02' => $this->t('February', [], ['context' => 'Long month name']),
      '03' => $this->t('March', [], ['context' => 'Long month name']),
      '04' => $this->t('April', [], ['context' => 'Long month name']),
      '05' => $this->t('May', [], ['context' => 'Long month name']),
      '06' => $this->t('June', [], ['context' => 'Long month name']),
      '07' => $this->t('July', [], ['context' => 'Long month name']),
      '08' => $this->t('August', [], ['context' => 'Long month name']),
      '09' => $this->t('September', [], ['context' => 'Long month name']),
      '10' => $this->t('October', [], ['context' => 'Long month name']),
      '11' => $this->t('November', [], ['context' => 'Long month name']),
      '12' => $this->t('December', [], ['context' => 'Long month name']),
    ];
    $element['year_between'] = [
      '#type' => 'select',
      '#options' => ['' => $this->t('-- Year --')] + array_combine($yearOptions, $yearOptions),
    ];
    $element['month_between'] = [
      '#type' => 'select',
      '#options' => ['' => $this->t('-- Month --')] + $monthOptions,
    ];
    $element['#attached']['drupalSettings']['bef_date_filters']['ids'][] = str_replace(search: '_', replace: '-', subject: $field_id);

    $element['min']['#attributes']['class'][] = 'visually-hidden';
    unset($element['min']['#title']);
    $element['max']['#attributes']['class'][] = 'visually-hidden';
    unset($element['max']['#title']);

  }

}
