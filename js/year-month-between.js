/**
 * @file
 * Provides Better Exposed Filter for a year and date filter.
 */

(function (drupalSettings, once) {
  Drupal.behaviors.befDateFilterYearMonthBetween = {
    attach: function (context, settings) {
      if (!drupalSettings.bef_date_filters) {
        return;
      }
      drupalSettings.bef_date_filters.ids.forEach(fieldId => {

        let [yearSelect] = once('befDateFilterYearMonthBetween', `[data-drupal-selector=edit-${fieldId}-year-between]`, context);
        let [monthSelect] = once('befDateFilterYearMonthBetween', `[data-drupal-selector=edit-${fieldId}-month-between]`, context);
        if (!yearSelect || !monthSelect) {
          return;
        }

        const minDate = document.querySelector(`[data-drupal-selector=edit-${fieldId}-min]`);
        const maxDate = document.querySelector(`[data-drupal-selector=edit-${fieldId}-max]`);

        yearSelect.addEventListener('change', (e) => {
          if (!e.target.value) {
            if (!monthSelect.value) {
              minDate.value = '';
              maxDate.value = '';
            }
            return;
          }
          if (monthSelect.value) {
            let nextMonth = parseInt(monthSelect.value) + 1;
            let year = parseInt(e.target.value);
            if (nextMonth === 13) {
              nextMonth = 1;
              year++;
            }
            minDate.value = `${e.target.value}-${monthSelect.value}-01`;
            maxDate.value = `${year}-${nextMonth.toString().padStart(2, '0')}-01`;
          }
          else {
            // When there is no month set the range to the whole year;
            minDate.value = `${e.target.value}-01-01`;
            maxDate.value = `${parseInt(e.target.value) + 1}-01-01`;
          }
        });

        monthSelect.addEventListener('change', (e) => {
          if (!e.target.value) {
            if (!yearSelect.value) {
              minDate.value = '';
              maxDate.value = '';
            }
            else {
              // Change to a year range.
              minDate.value = `${yearSelect.value}-01-01`;
              maxDate.value = `${parseInt(yearSelect.value) + 1}-01-01`;
            }
            return;
          }
          // When there is no year set it to the current year;
          if (!yearSelect.value) {
            yearSelect.value = (new Date().getFullYear());
          }
          let nextMonth = parseInt(e.target.value) + 1;
          let year = parseInt(yearSelect.value);
          if (nextMonth === 13) {
            nextMonth = 1;
            year++;
          }
          minDate.value = `${yearSelect.value}-${e.target.value}-01`;
          maxDate.value = `${year}-${nextMonth.toString().padStart(2, '0')}-01`;
        });
      });
    }
  };
})(drupalSettings, once);
